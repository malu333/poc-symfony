<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Offer;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;



class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..') . DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/createoffer", name="create_offer")
     */
    public function createAction()
    {
        $offer = new Offer();
        $offer->setName('Keyboard2');

        $em = $this->getDoctrine()->getManager();

        // tells Doctrine you want to (eventually) save the Product (no queries yet)
        $em->persist($offer);

        // actually executes the queries (i.e. the INSERT query)
        $em->flush();

        return new Response('Saved new product with id ' . $offer->getId());
    }

    /**
     * @Route("/home", name="home")
     */
    public function homeAction()
    {
        $repository = $this->getDoctrine()
            ->getRepository('AppBundle:Offer');
        $offers = $repository->findAll();

        $offer = new Offer();
        $form = $this->createFormBuilder($offer)
            ->add('name', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Create offer'))
            ->getForm();
        return $this->render('offers/home.html.twig', array(
            'form' => $form->createView(),
            'offers' => $offers
        ));
    }

    /**
     * @Route("/ajaxlist", name="ajaxlist")
     */
    public function listAjaxAction(){
        $repository = $this->getDoctrine()
            ->getRepository('AppBundle:Offer');
        $offers = $repository->findAll();

        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());

        $serializer = new Serializer($normalizers, $encoders);
        //prepare the response, e.g.
        $offersJson = $serializer->serialize($offers, 'json');

        //you can return result as JSON
        $res = new Response($offersJson);
        $res->headers->set('Content-Type', 'application/json');

        return $res;
    }


    public function list2()
    {
        $repository = $this->getDoctrine()
            ->getRepository('AppBundle:Offer');
        $offers = $repository->findAll();
        $html = $this->renderView('offers/list.html.twig', array(
            'offers' => $offers,
        ));
        return $html;

    }

    /**
     * @Route("/newoffer", name="new_offer")
     */
    public function newAction(Request $request)
    {
        $offer = new Offer();
        //$request = $this->container->get('request');
        // var_dump($request->request->all());
        // var_dump($request->request->all());
        $name = $request->request->get('name');
        $offer->setName($name);
        $em = $this->getDoctrine()->getManager();
        $em->persist($offer);

        $em->flush();

        //prepare the response, e.g.
        $response = array("code" => 200, "success" => true, "html" => $this->list2());
        //you can return result as JSON
        return new Response(json_encode($response));
    }

}
